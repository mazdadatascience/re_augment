#%%
import os
import glob
import subprocess
import mazdap as mz
from sqlalchemy import create_engine
import credentials
import pandas as pd

conn = mz.ObieeConnector(uid=credentials.uid,pwd=credentials.pwd)

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV"
)


def get_fact_sales():
    sls_query = """\
   SELECT
      "Vehicle - Sales"."Sales Calendar & Timing"."Sales Year Month ID" sls_yr_mo_id,
      "Vehicle - Sales"."Dealer Detail"."Dealer Cd" dlr_cd,
      "Vehicle - Sales"."Facts"."GVS Retail" gvs_retail
   FROM "Vehicle - Sales"
   WHERE
   (("Retail Region"."Retail Country Cd" = 'US') AND ("Sales Calendar & Timing"."Sales Year" >= 2017))"""

    cpo_query = """\
   SELECT
      "Vehicle - Sales"."Sales Calendar & Timing"."Sales Year Month ID" sls_yr_mo_id,
      "Vehicle - Sales"."Dealer Detail"."Dealer Cd" dlr_cd,
      "Vehicle - Sales"."Facts"."CPO Vehicle Sales" cpo_sales
   FROM "Vehicle - Sales"
   WHERE
   (("Retail Region"."Retail Country Cd" = 'US') AND ("Sales Calendar & Timing"."Sales Year" >= 2017))"""

    sls_df = conn.exec_sql(
        sls_query, custom_cols=["sls_yr_mo_id", "dlr_cd", "gvs_retail"]
    )
    cpo_df = conn.exec_sql(
        cpo_query, custom_cols=["sls_yr_mo_id", "dlr_cd", "cpo_sales"]
    )


    sls_df['date'] = pd.to_datetime(sls_df['sls_yr_mo_id'], format='%Y%m')
    cpo_df['date'] = pd.to_datetime(cpo_df['sls_yr_mo_id'], format='%Y%m')
    sls_df["gvs_retail"] = sls_df["gvs_retail"].astype(int)
    cpo_df["cpo_sales"] = cpo_df["cpo_sales"].astype(int)

    sls_df.to_csv("data/fact_sales.csv", index=False)
    cpo_df.to_csv("data/fact_cpo.csv", index=False)

# TODO: Pull from OS tables
def get_ub_share():
    query = """\
WITH soa AS (
    SELECT *
    FROM OS_UB_SOA_DAILY
    UNION ALL
    SELECT *
    FROM OS_UB_SOA_2020
    UNION ALL
    SELECT *
    FROM OS_UB_SOA_2019
    UNION ALL
    SELECT *
    FROM OS_UB_SOA_2018
)

SELECT
    kcm.SALES_YR_MO_ID,
    DEALER_CODE,
    MAKE,
    SUM(SALES) AS SALES
FROM
(
    SELECT
        soa.VEHICLE_SALES_DAY,
        soa.DEALER_CODE,
    (
    CASE
        WHEN soa.MAKE <> 'MAZDA' THEN 'OTHERS'
        ELSE 'MAZDA'
        END)
        AS MAKE,
    (
    CASE
        WHEN vvs.MODEL NOT IN ('M3H', 'M3S', 'CX3', 'C30', 'CX5', 'CX9', 'M6G') THEN 'OTHERS'
        ELSE vvs.MODEL
        END)
        AS MODEL,
    vvs.SEGMENT,
    soa.SALES
    FROM soa
    LEFT JOIN RPR_STG.VIEW_VEH_SEGMENTATION  vvs ON soa.MAKE = vvs.BRAND
    AND (
        CASE
            soa.MODEL WHEN 'mazda3 hatchback' THEN 'm3h'
            WHEN 'mazda3 sedan' THEN 'm3s'
            WHEN 'cx-3' THEN 'cx3'
            WHEN 'cx-30' THEN 'c30'
            WHEN 'cx-5' THEN 'cx5'
            WHEN 'cx-9' THEN 'cx9'
            WHEN 'mazda6' THEN 'm6g'
            ELSE vvs.MODEL
        END
        ) = vvs.MODEL
    AND soa.MODEL_YEAR = vvs.MODELYEAR
    LEFT JOIN RPR_STG.KHN_CALENDAR_MASTER kcm ON soa.VEHICLE_SALES_DAY = kcm.CAL_DATE
    WHERE
        soa.VEHICLE_SALES_DAY >= DATE '2017-01-04'
        AND vvs.SEGMENT IN ('C CAR', 'CD CAR', 'C UTILITY', 'B UTILITY', 'MIDSIZE UTILITY 3 ROW', 'SPORTS CAR')
        AND soa.RETAIL_FLEET = 'RETAIL'
        AND soa.DEALER_CODE <> 'NA'
    ) soa_tb
LEFT JOIN RPR_STG.KHN_CALENDAR_MASTER kcm ON soa_tb.VEHICLE_SALES_DAY = kcm.CAL_DATE
GROUP BY
    kcm.SALES_YR_MO_ID,
    DEALER_CODE,
    MAKE
""".strip()


    soa_share_df = pd.read_sql(query, engine)
    soa_share_df['date'] = pd.to_datetime(soa_share_df['sales_yr_mo_id'], format='%Y%m')
    soa_share_df.to_csv("data/fact_soa_share.csv", index=False)

def get_cx():
    query = """\
    SELECT 
        dlr_cd, questions, response, response_dt
    FROM 
        TM_CX360_SALES_SERVICE """.strip()

    cx_df = pd.read_sql(query, engine)
    cx_df['date'] = pd.to_datetime(cx_df['response_dt'], format='%Y%m%d')
    cx_df.to_csv("data/fact_cx.csv", index=False)

def get_fin():
    fin_query = """\
SELECT
   "Finance - Dealer"."Date"."Calendar Year Month ID",
   "Finance - Dealer"."Dealer"."Dealer Code",
   "Finance - Dealer"."KPI - Excl R12"."T01 - Net Profit BBIT % Sales (ROS) - R12",
   "Finance - Dealer"."KPI - Excl R12"."T02 - Net Profit BBIT $ - R12"
FROM "Finance - Dealer"
WHERE
("Date"."Calendar Year Month ID" >= 201801)""".strip()
    fin_df = conn.exec_sql(fin_query, custom_cols=["cal_yr_mo_id", "dlr_cd", "ros_r12", "profit_r12"])
    fin_df['date'] = pd.to_datetime(fin_df['cal_yr_mo_id'], format='%Y%m')
    fin_df.to_csv("data/fact_fin.csv", index=False)

def get_cpro():
    query = """\
SELECT
   "Vehicle - Repair Order Claim"."Date - Repair Order Completion"."RO Completion Calendar Year Month ID" s_1,
   "Vehicle - Repair Order Claim"."Dealer - Repair Order Claim"."Dealer Code" s_2,
   "Vehicle - Repair Order Claim"."Facts"."Cust Total Amt" s_3,
   "Vehicle - Repair Order Claim"."Facts"."RO Count" s_4
FROM "Vehicle - Repair Order Claim"
WHERE
(("Dealer Hierarchy"."RO Country Code" = 'US') AND ("Facts"."Service Pay Type" = 'C') AND ("Date - Repair Order Completion"."RO Completion Calendar Year Month ID" >= 201901))""".strip()
    cpro_df = conn.exec_sql(query, custom_cols=["cal_yr_mo_id", "dlr_cd", "customer_pay", "ro_count"])
    cpro_df['date'] = pd.to_datetime(cpro_df['cal_yr_mo_id'], format='%Y%m')
    cpro_df.to_csv("data/fact_cpro.csv", index=False)

# TODO: Pull from EDW/FTP
#TODO Update with the new information
def get_ihs():

    #https://loyalty.polk.com/loyalty/VER2.4-SNAPSHOT/index2.html#!/analysis
    # Download from ihs and upload into file
    # ! Powerbi does processing
    
    # ihs_df = pd.read_sql("SELECT * FROM AT_IHS_REAR_DATA", engine)
    # ihs_df['date'] = pd.to_datetime(ihs_df['time_period'], format='%Y-%m')
    # ihs_df.to_csv("data/fact_ihs_rear.csv", index=False)
    return

# TODO: Pull the dealer_history_snapshot.txt - Larry Ochs
# Tables that need to be refreshed from mainframe
# Dealer History Snapshot: BTC02450
def get_dealer_history():
	# query = """\
	# SELECT * FROM EDW_STG.BTC02450_DLR_MSTR_HIST
	# """
	# dlr_df = pd.read_sql(query, engine)
    
    # dlr_df = pd.read_csv('data/dealer_master_snapshot_input.csv')

    dlr_df = pd.read_csv('data/dealer_master_snapshot.csv')

    dlr_df_append = conn.get_csv('/shared/Operational Strategy/Governance and Process Innovation/Mazdap/All Dealers RE Augment')

    dlr_df_append = dlr_df_append.rename(columns = {
        'City Name':'CITY_NM',
        'Dealer Name':'DBA_NM' ,
        'District Code':'DSTRCT_CD' ,
        'Dealer Code':'DLR_CD' ,
        'Dealer Dual SSR Flag':'DLR_DUAL_SSR_FL' ,
        'Dealer Type Code':'DLR_TYPE_CD' ,
        'Facility Type Code':'FCLTY_TYPE_CD' ,
        'Financial Statement Type Code':'FNCSTMT_TYPE_CD' ,
        'MDA Code':'MDA_CD' ,
        'MDA Name':'MDA_NM' ,
        'Metro Code':'METRO_CD' ,
        'Region Code':'RGN_CD' ,
        'SOA Code':'SOA_CD' ,
        'SOA Name':'SOA_NM' ,
        'St Cd':'ST_CD' ,
        'Status Code':'STATUS_CD' ,
        'Zip Code 1':'ZIP1_CD'
    })
    dlr_df_append['YRMO_END_DT'] = '202202'
    
    
    dlr_df = pd.concat([dlr_df, dlr_df_append])
    dlr_df = dlr_df.drop_duplicates()
    # dlr_cols = dlr_df.columns.str.lower()
    # dlr_df.columns = dlr_cols
    
    dlr_df.to_csv("data/dealer_master_snapshot.csv", index=False)

# TODO: Pull phase history
# Tables that need to be refreshed from mainframe
# RE Phases / Date : BTC06230



def get_market_share():
    df = conn.get_csv('/shared/Operational Strategy/Governance and Process Innovation/Mazdap/mazda_segment_sales_by_market_month')
    df['Sales Year Month Id'] = df['Sales Year Month Id'].astype(float).astype(int).astype(str)
    df['Date'] = pd.to_datetime(df['Sales Year Month Id']+'01', format="%Y%m%d")
    df.to_csv('data/fact_dcs_market.csv', index=False)


# TODO: Pull Service Retention + Brand Retention
# TODO: Request table refresh
def get_service_retention():
    
    service_retention = pd.read_sql(
        """SELECT base.*
            FROM (
                 SELECT TO_DATE(REPORT_DATE, 'MM-YYYY') AS RETENTION_DATE,
                    DEALER_REGION_DIST,
                    SERVICED_DLR,
                    UIO,
                    SERVICED_BRAND,
                    DEALER_RETENTION,
                    RETENTION_OTHER_DLR,
                    BRAND_RETENTION
                 FROM EDW_STG.UB_SERVICE_RETENTION
             ) base
            ORDER BY DEALER_REGION_DIST, RETENTION_DATE DESC""", engine
    )

    spi_file_columns = ["granularity",
                        "retention_date",
                        "dealer_region_dist",
                        "serviced_dlr",
                        "uio",
                        "dealer_retention",
                        "serviced_dlr_2",
                        "uio_2",
                        "dealer_retention_2",
                        "avoid_1",
                        "retention_other_dlr",
                        "serviced_brand",
                        "brand_retention"]

    spi_files = glob.glob(os.path.join('/app/tmarx/development/re_augment/data_input/', "*.txt"))

    spi_df_list = []
    for file in spi_files:
        df = pd.read_csv(file, header=None, delimiter="|")
        # print("File: {} has {} rows".format(file, len(df)))
        spi_df_list.append(df)
    spi_file_df = pd.concat(spi_df_list, axis=0)
    # print("Final dataframe before filtering has {} rows.".format(len(spi_file_df)))

    spi_df_concat = spi_file_df.iloc[:, :13]
    spi_df_concat.columns = spi_file_columns

    # Filter based on granularity = 5, and then remove unneeded columns (using original list in service_retention df)
    spi_df_concat = spi_df_concat[spi_df_concat.granularity == 5]
    spi_df_concat = spi_df_concat[list(service_retention.columns)]

    # Fix data types
    spi_df_concat.retention_date = pd.to_datetime(spi_df_concat.retention_date, format='%m-%Y')

    # Concatenate and remove duplicates
    service_retention = pd.concat([service_retention, spi_df_concat], ignore_index=True)
    service_retention = service_retention.drop_duplicates()



    service_retention['month'] = [str(x.month).zfill(2) for x in service_retention['retention_date']]
    service_retention['sales_yr_mo_id'] = service_retention['retention_date'].dt.year.astype(str) + service_retention['month']


    service_retention.to_csv('data/fact_retention.csv', index=False)




# TODO; Pull employee turnover (r12) - Foon
# TODO: Update IHS Data Feed



# get_fact_sales()
# get_ub_share()
# get_cx()
#TODO Update Finance!
# get_fin()
# get_service_retention()
# get_market_share()
# get_dealer_history()
# get_cpro()
# get_ihs()

#### SYNC ONEDRIVE ####
rclone_directory = 'onedrive_operational_strategy'
sync_cmd = f"rclone sync -v data {rclone_directory}:wphyo/region_reporting_re --ignore-size --ignore-checksum --update"

# Need to extract service retention files from sharepoint
copy_files = f"rclone sync -v {rclone_directory}:pbi_data/abp_input/spi /app/tmarx/development/re_augment/data_input/ --ignore-size --ignore-checksum --update"


with open("logging.txt", "w") as f:
    process = subprocess.Popen(sync_cmd, shell=True, stderr=f, universal_newlines=True)
    process2 = subprocess.Popen(copy_files, shell=True, stderr=f, universal_newlines=True)
    while True:
        
        return_code = process.poll()
        return_code2 = process2.poll()
        if return_code is not None:
            break
#### SYNC ONEDRIVE ####

conn.logoff()
# %%
