#%%
import pandas as pd
import mazdap
import os
from sqlalchemy import create_engine
import credentials
from sqlalchemy.types import DateTime, String
from datetime import datetime

con = mazdap.ObieeConnector(uid=credentials.uid, pwd=credentials.pwd)
os.environ["NLS_LANG"] = ".AL32UTF8"
engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV"
)

###### Initialize table ######
# %%
# def get_cx():
#     query = """\
# SELECT
# 	DLR_CD,
# 	QUESTIONS,
# 	RESPONSE,
# 	RESPONSE_DT
# FROM
# 	WP_CX360_SERVICE_RAW wcsr
# WHERE
# 	INFLUENCE = 'N'
# 	AND OWNERSHIP = 'Y'
# 	AND QUESTIONS IN ('Service Likelihood to Return', 'Service Overall Satisfaction')
# UNION ALL
# SELECT
# 	DLR_CD,
# 	QUESTIONS,
# 	RESPONSE,
# 	RESPONSE_DT
# FROM
# 	WP_CX360_SALES_RAW wcsr
# WHERE
# 	INFLUENCE = 'N'
# 	AND OWNERSHIP = 'Y'
# 	AND QUESTIONS IN ('Sales Likelihood to Recommend', 'Sales Overall Satisfaction')""".strip()

#     cx_df = pd.read_sql(query, engine)
#     return cx_df
# #%%
# df = get_cx()

# df_dtypes = {
# 'dlr_cd':String(30),
# 'questions':String(60),
# 'response':String(60),
# 'response_dt': DateTime(),
# }
# df.to_sql(name='TM_CX360_SALES_SERVICE', con=engine, if_exists='replace',
# dtype=df_dtypes, index=False)


####### Add to dataframe ########
#%%
query = """\
    SELECT 
        MAX(response_dt)
    FROM
        TM_CX360_SALES_SERVICE"""

max_date = engine.execute(query).fetchall()

max_date = max_date[0][0].strftime("%Y-%m-%d")

#%%
def pull_cx(cx_type=None, cx_date='2021-04-30'):

    if cx_type == None:
        print("Please specify 'Sales' or 'Service'")
        return

    cx_query = f"""\
    SELECT
    "CX - CX360 Survey Detail"."Dealer"."Dealer Code" DLR_CD,
    "CX - CX360 Survey Detail"."Fact - Customer Survey"."Response" RESPONSE,
    "CX - CX360 Survey Detail"."Response Date"."Calendar Date" RESPONSE_DT,
    "CX - CX360 Survey Detail"."Survey"."Questions" QUESTIONS
    FROM 
        "CX - CX360 Survey Detail"
    WHERE
        (("Survey"."Survey Type" IN ('{cx_type}')) 
        AND ("Fact - Customer Survey"."Response" IS NOT NULL) 
        AND ("Response Date"."Calendar Date" >= date '{cx_date}') 
        AND ("Dealer"."Country Code" = 'US') AND ("Product"."Brand" = 'MAZDA') 
        AND ("Fact - Customer Survey"."Influence Flag" = 'N') 
        AND ("Fact - Customer Survey"."Ownership Flag" = 'Y') 
        AND ("Survey"."Questions" IN ('Sales Likelihood to Recommend', 'Service Likelihood to Return', 'Service Overall Satisfaction', 'Sales Overall Satisfaction')
        ))
    """

    cx_df = con.exec_sql(
        cx_query
    )
    return cx_df



#%%
# Pull data based on most recent
service_cx = pull_cx(cx_type='Service', cx_date=max_date )
sales_cx = pull_cx(cx_type='Sales', cx_date=max_date )


#%%
# Data cleanup
service_cx.columns = service_cx.columns.str.lower().str.replace(' ','_')
sales_cx.columns = sales_cx.columns.str.lower().str.replace(' ','_')

service_cx.rename(columns={'calendar_date':'response_dt',
                            'dealer_code':'dlr_cd'}, inplace=True)
sales_cx.rename(columns={'calendar_date':'response_dt',
                            'dealer_code':'dlr_cd'}, inplace=True)

service_cx['response_dt'] = pd.to_datetime(service_cx['response_dt'], format='%Y-%m-%d')
sales_cx['response_dt'] = pd.to_datetime(sales_cx['response_dt'], format='%Y-%m-%d')
service_cx['dlr_cd'] = service_cx['dlr_cd'].astype(str)
sales_cx['dlr_cd'] = sales_cx['dlr_cd'].astype(str)
#%%
service_cx.to_csv('/app/tmarx/development/re_augment/data/service_temp.csv', index=False)
sales_cx.to_csv('/app/tmarx/development/re_augment/data/sales_temp.csv', index=False)
#%%

# service_cx = pd.read_csv('/app/tmarx/development/re_augment/data/service_temp.csv')
# sales_cx = pd.read_csv('/app/tmarx/development/re_augment/data/sales_temp.csv')

# service_cx.rename(columns={
#                             'dealer_code':'dlr_cd'}, inplace=True)
# sales_cx.rename(columns={
#                             'dealer_code':'dlr_cd'}, inplace=True)
# service_cx['response_dt'] = pd.to_datetime(service_cx['response_dt'], format='%Y-%m-%d')
# sales_cx['response_dt'] = pd.to_datetime(sales_cx['response_dt'], format='%Y-%m-%d')
# service_cx['dlr_cd'] = service_cx['dlr_cd'].astype(str)
# sales_cx['dlr_cd'] = sales_cx['dlr_cd'].astype(str)

#%%

df_dtypes = {
'dlr_cd':String(30),
'questions':String(100),
'response':String(256),
'response_dt': DateTime(),
}


service_cx.to_sql(name='TM_TEMP_CX360_SERVICE', con=engine,
     if_exists='replace', dtype=df_dtypes, index=False)
sales_cx.to_sql(name='TM_TEMP_CX360_SALES', con=engine, 
    if_exists='replace', dtype=df_dtypes, index=False)

# Merge tables
#%% 
with engine.begin() as cn:
   sql = """INSERT INTO TM_CX360_SALES_SERVICE 
            SELECT t.dlr_cd, t.questions, t.response, t.response_dt
            FROM TM_TEMP_CX360_SERVICE t
            WHERE NOT EXISTS 
                (SELECT 1 FROM TM_CX360_SALES_SERVICE f
                 WHERE t.dlr_cd = f.dlr_cd
                 AND t.response = f.response
                 AND t.response_dt = f.response_dt
                 AND t.questions = f.questions)"""

   cn.execute(sql)

with engine.begin() as cn:
   sql = """INSERT INTO TM_CX360_SALES_SERVICE 
            SELECT t.dlr_cd, t.questions, t.response, t.response_dt
            FROM TM_TEMP_CX360_SALES t
            WHERE NOT EXISTS 
                (SELECT 1 FROM TM_CX360_SALES_SERVICE f
                 WHERE t.dlr_cd = f.dlr_cd
                 AND t.response = f.response
                 AND t.response_dt = f.response_dt
                 AND t.questions = f.questions)"""

   cn.execute(sql)



# #%%
# df_dtypes = {
# 'dlr_cd':String(30),
# 'questions':String(60),
# 'response':String(60),
# 'response_dt': DateTime(),
# 'date': DateTime()
# }
# cx_df.to_sql(name='TM_CX360_SALES_SERVICE', con=engine, if_exists='replace',
# dtype=df_dtypes, index=False)
# # %%


# %%
